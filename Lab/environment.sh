#!/bin/bash

case `uname` in
    "Darwin")
        # IP_ADDRESS=`ifconfig | grep netmask | grep broadcast | sed 's/^.*inet //g' | sed 's/ netmask.*$//g' | tr -cd '[:print:]'`
        IP_ADDRESS=192.168.2.10
        ;;
    "Linux")
        IP_ADDRESS=`ip address show dev enp0s3 | grep "inet " | sed 's/^.*inet //g' | sed 's/\/.*$//g' | tr -cd '[:print:]'`
        # IP_ADDRESS=10.0.2.15
        ;;
esac

echo "My address is -${IP_ADDRESS}-"
