#!/bin/bash

function unpack() {
    local PACKAGE=$1
    local DIR=$2
    local PACKAGES="$PWD/../../Packages"
    
    if [ ! -d "$DIR" ]; then
        echo "Unpacking $PACKAGE in $DIR ..."
        unzip -q $PACKAGES/$PACKAGE
    fi
}

unpack migrationtoolkit-rhamt-cli-4.3.1-light.zip rhamt-cli-4.3.1.Final
unpack windup-quickstarts-4.3.1.Final.zip windup-quickstarts-4.3.1.Final
unpack windup-sample-apps.zip windup-sample-apps

set -x
RHAMT_HOME="$STUDENT_HOME/Software/rhamt-cli-4.3.1.Final"

$RHAMT_HOME/bin/rhamt-cli "$@"
