#!/bin/sh

function unpack() {
    local PACKAGE=$1
    local DIR=$2
    local PACKAGES="$PWD/../../Packages"
    
    if [ ! -d "$DIR" ]; then
        echo "Unpacking $PACKAGE in $DIR ..."
        unzip -q $PACKAGES/$PACKAGE
    fi
}

unpack jboss-eap-6.4.0.zip jboss-eap-6.4
unpack jboss-eap-7.3.0.zip jboss-eap-7.3

set -x
JBOSS_HOME="$PWD/jboss-eap-7.3"

$JBOSS_HOME/bin/jboss-server-migration.sh "$@"
