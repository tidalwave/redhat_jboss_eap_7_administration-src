#!/bin/sh

. "$PWD/../environment.sh"

CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --connect
jms-queue add --queue-address=myQueue --durable=false --entries=queue/myQueue,java:jboss/exported/jms/queue/myQueue
:reload
EOF
echo
