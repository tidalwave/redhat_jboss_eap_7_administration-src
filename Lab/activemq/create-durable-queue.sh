#!/bin/sh

. "$PWD/../environment.sh"

CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --connect
jms-queue remove --queue-address=myQueue
jms-queue add --queue-address=myQueue --durable=true --entries=queue/myQueue,java:jboss/exported/jms/queue/myQueue
/subsystem=messaging-activemq/server=default:write-attribute(name=journal-file-size, value=9728)
/subsystem=messaging-activemq/server=default:write-attribute(name=journal-min-files,value=3)
:reload
EOF
echo
