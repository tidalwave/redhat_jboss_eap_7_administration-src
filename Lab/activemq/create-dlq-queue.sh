#!/bin/sh

. "$PWD/../environment.sh"

CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --connect
jms-queue add --queue-address=myQueueDLQ --durable=false --entries=queue/myQueueDLQ,java:jboss/exported/jms/queue/myQueueDLQ
cd subsystem=messaging-activemq
cd server=default
cd address-setting
./jms.queue.myQueue:write-attribute(name=dead-letter-address, value=jms.queue.myQueueDLQ)
./jms.queue.myQueue:write-attribute(name=max-delivery-attempts, value=5)
EOF
echo
