#!/bin/sh

. "$PWD/../environment.sh"

CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --connect
jms-queue add --queue-address=myQueueExp --durable=false --entries=queue/myQueueExp,java:jboss/exported/jms/queue/myQueueExp
cd subsystem=messaging-activemq
cd server=default
cd address-setting
./jms.queue.myQueue:add(expiry-address=jms.queue.myQueueExp)
EOF
echo
