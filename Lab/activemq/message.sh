#!/bin/sh

URL=http-remoting://127.0.0.1:8080
USERNAME=jms
PASSWORD=jms123
QUEUE=jms/queue/myQueue

COMMAND=$1
MESSAGE=$2

java -Djmsclient.url=$URL -Djmsclient.userName=$USERNAME -Djmsclient.password=$PASSWORD \
-Djmsclient.queueName=$QUEUE -Djmsclient.command=$COMMAND -Djmsclient.message=$MESSAGE -jar jms-client.jar 

