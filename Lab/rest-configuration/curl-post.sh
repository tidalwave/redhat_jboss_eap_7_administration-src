#!/bin/sh

JSON=$1

set -x

curl -X POST -v --digest -u admin:admin -v http://localhost:9990/management -H"Content-Type: application/json" --data "@$JSON"
