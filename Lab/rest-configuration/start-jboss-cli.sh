#!/bin/sh

if [ ! -d "$PWD/standalone" ]; then
    echo "Creating a local copy of standalone base dir..."
    cp -r "$JBOSS_HOME/standalone" "$PWD/standalone"
fi

set -x
$JBOSS_HOME/bin/jboss-cli.sh -Djboss.server.base.dir="$PWD/standalone" "$@"
