#!/bin/sh

. "$PWD/../environment.sh"

set -x
$JBOSS_HOME/bin/standalone.sh \
-Djboss.server.base.dir="$PWD/balancer/standalone" \
-Djboss.domain.master.address=$IP_ADDRESS \
-Djboss.bind.address.management=$IP_ADDRESS \
-Djboss.bind.address=$IP_ADDRESS \
-Djboss.socket.binding.port-offset=1000 \
-c=standalone-load-balancer.xml 
