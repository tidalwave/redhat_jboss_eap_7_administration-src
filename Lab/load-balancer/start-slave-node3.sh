#!/bin/sh

. "$PWD/../environment.sh"

set -x
$JBOSS_HOME/bin/domain.sh \
-Djboss.domain.base.dir="$PWD/node3/domain" \
-Djboss.domain.master.address=$IP_ADDRESS \
-Djboss.management.http.port=9993 \
-Djboss.bind.address.management=$IP_ADDRESS \
-Djboss.bind.address=$IP_ADDRESS \
--host-config=host-slave.xml
