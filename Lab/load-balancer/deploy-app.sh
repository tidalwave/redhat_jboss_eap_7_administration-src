#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
APP=$1
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --controller=$CONTROLLER --connect
deploy "$PWD/$1" --server-groups=other-server-group 
exit
EOF
