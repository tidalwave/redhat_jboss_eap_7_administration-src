#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

$CLI --connect --controller=$CONTROLLER "$@"
