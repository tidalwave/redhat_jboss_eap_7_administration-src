#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS:10990
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

$CLI --connect --controller=$CONTROLLER "$@"
