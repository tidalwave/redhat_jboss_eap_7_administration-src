#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS:10990
CLI=$JBOSS_HOME/bin/jboss-cli.sh
HOST2_IP_ADDRESS=$IP_ADDRESS
HOST3_IP_ADDRESS=$IP_ADDRESS

set -x

WEBPATH=/app
HANDLER=app-handler

cat << EOF | $CLI --controller=$CONTROLLER --connect
cd /socket-binding-group=standard-sockets
./remote-destination-outbound-socket-binding=host2-server-one/:add(host=$HOST2_IP_ADDRESS, port=8009)
./remote-destination-outbound-socket-binding=host3-server-one/:add(host=$HOST3_IP_ADDRESS, port=8309)

cd /subsystem=undertow/configuration=handler/reverse-proxy=$HANDLER
./host=host2:add(outbound-socket-binding=host2-server-one, scheme=ajp, instance-id=myroute3, path=$WEBPATH)
./host=host3:add(outbound-socket-binding=host3-server-one, scheme=ajp, instance-id=myroute4, path=$WEBPATH)

/:reload
EOF
