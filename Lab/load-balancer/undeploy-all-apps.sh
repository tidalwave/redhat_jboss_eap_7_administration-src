#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
APP=$1
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --controller=$CONTROLLER --connect
undeploy app.war
exit
EOF
