#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS:10990
CLI=$JBOSS_HOME/bin/jboss-cli.sh
HOST2_IP_ADDRESS=$IP_ADDRESS
HOST3_IP_ADDRESS=$IP_ADDRESS

set -x

WEBPATH=/app
HANDLER=app-handler

cat << EOF | $CLI --controller=$CONTROLLER --connect
cd subsystem=undertow/configuration=handler
./reverse-proxy=$HANDLER:add

cd /socket-binding-group=standard-sockets
./remote-destination-outbound-socket-binding=host2-server-two/:add(host=$HOST2_IP_ADDRESS, port=8159)
./remote-destination-outbound-socket-binding=host3-server-two/:add(host=$HOST3_IP_ADDRESS, port=8459)

cd /subsystem=undertow/configuration=handler/reverse-proxy=$HANDLER
./host=host2:add(outbound-socket-binding=host2-server-two, scheme=ajp, instance-id=myroute1, path=$WEBPATH)
./host=host3:add(outbound-socket-binding=host3-server-two, scheme=ajp, instance-id=myroute2, path=$WEBPATH)

cd /subsystem=undertow/server=default-server/host=default-host
./location=\\$WEBPATH:add(handler=$HANDLER)
/:reload
EOF
