#!/bin/bash

set -x
rm -rf balancer node1 node2 node3
mkdir balancer node1 node2 node3
cp -r $JBOSS_HOME/standalone balancer
cp -r $JBOSS_HOME/domain node1
cp -r $JBOSS_HOME/domain node2
cp -r $JBOSS_HOME/domain node3
chmod -R ug+w balancer node1 node2 node3
patch node2/domain/configuration/host-slave.xml node2.patch 
patch node3/domain/configuration/host-slave.xml node3.patch 

######## Uncomment this if your machine has less than 6 GB of RAM
#patch node1/domain/configuration/domain.xml domain-xml-memory.patch
