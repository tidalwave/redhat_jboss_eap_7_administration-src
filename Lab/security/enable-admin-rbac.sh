#!/bin/sh

. "$PWD/../environment.sh"

CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat << EOF | $CLI --connect
/core-service=management/access=authorization/role-mapping=SuperUser/include=admin:add(name=admin, type=USER)
/:reload
EOF
echo
