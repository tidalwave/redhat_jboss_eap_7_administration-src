#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
HOST=$1
SERVER=$2
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

$CLI -c --controller=$CONTROLLER --command="/host=$HOST/server-config=$SERVER:stop(blocking=true)"
$CLI -c --controller=$CONTROLLER --command="/host=$HOST/server-config=$SERVER:remove()"
