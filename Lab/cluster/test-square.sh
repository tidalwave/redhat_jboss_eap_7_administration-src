#!/bin/bash

. "$PWD/../environment.sh"
SERVER1=$IP_ADDRESS:8230
SERVER2=$IP_ADDRESS:8530

echo "SERVER1 is $SERVER1"
echo "SERVER2 is $SERVER2"

function computeSquare() {
    local SERVER=$1
    local N=$2
    local URL=http://$SERVER/square/rest/calc/$N
    echo "Requesting $URL ..."
    curl -s $URL
    echo
    curl -s http://$SERVER/square/rest/stats
}

echo "================================================================"
echo "Asking for 4 from first server - could be not in cache"
computeSquare $SERVER1 4

echo "================================================================"
echo "Asking again for 4 from first server  - should be in cache"
computeSquare $SERVER1 4

echo "================================================================"
echo "Asking again for 4 from first server  - should be in cache"
computeSquare $SERVER1 4

echo "================================================================"
echo "Asking for 4 from second server - should be in cache"
computeSquare $SERVER2 4

echo "================================================================"
echo "Asking for 5 from second server - could be not in cache"
computeSquare $SERVER2 5

echo "================================================================"
echo "Asking for 5 from first server - should be in cache"
computeSquare $SERVER1 5
