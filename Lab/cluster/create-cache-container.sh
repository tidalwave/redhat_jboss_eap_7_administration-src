#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat | $CLI --connect --controller=$CONTROLLER << EOF
cd profile=full-ha
cd subsystem=infinispan
./cache-container=calccontainer:add()
cd cache-container=calccontainer
./transport=TRANSPORT:add(lock-timeout=60000)
./replicated-cache=calccache:add(mode=SYNC)
./:write-attribute(name=default-cache,value=calccache)
/:reload-servers
exit

EOF
