#!/bin/sh

. "$PWD/../environment.sh"

set -x
$JBOSS_HOME/bin/domain.sh \
-Djboss.domain.base.dir="$PWD/node1/domain" \
-Djboss.domain.master.address=$IP_ADDRESS \
-Djboss.bind.address.management=$IP_ADDRESS \
-Djboss.bind.address=$IP_ADDRESS \
--host-config=host-master.xml 
