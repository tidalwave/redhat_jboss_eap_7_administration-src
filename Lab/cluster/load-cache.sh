#!/bin/bash

. "$PWD/../environment.sh"
SERVER1=$IP_ADDRESS:8230
SERVER2=$IP_ADDRESS:8530
COUNT=$1

echo "SERVER1 is $SERVER1"
echo "SERVER2 is $SERVER2"

for (( i = 1; i <= $COUNT; i++ )); do
    URL=http://$SERVER1/square/rest/calc/$i?fast=true
    echo $URL
    curl -s $URL 
    echo
done

curl -s http://$SERVER1/square/rest/stats
