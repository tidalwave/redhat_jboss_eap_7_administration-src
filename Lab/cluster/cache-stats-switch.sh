#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
SWITCH=$1
CLI=$JBOSS_HOME/bin/jboss-cli.sh

cat | $CLI -c --controller=$CONTROLLER << EOF
cd profile=full-ha
cd subsystem=infinispan
cd cache-container=calccontainer
:write-attribute(name=statistics-enabled,value=$SWITCH)
cd replicated-cache=calccache
:write-attribute(name=statistics-enabled,value=$SWITCH)
/:reload-servers
exit

EOF
