#!/bin/sh

. "$PWD/../environment.sh"
SERVER=$IP_ADDRESS:8230

echo "Requesting http://$SERVER/square/rest/clear ..."
curl -s http://$SERVER/square/rest/clear
echo
