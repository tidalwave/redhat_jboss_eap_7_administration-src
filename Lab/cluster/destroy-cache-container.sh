#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

cat | $CLI --connect --controller=$CONTROLLER << EOF
undeploy square.war --server-groups=other-server-group 
cd profile=full-ha
cd subsystem=infinispan
./cache-container=calccontainer:remove()
/:reload-servers
exit

EOF
