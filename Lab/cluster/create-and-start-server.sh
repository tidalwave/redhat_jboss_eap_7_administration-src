#!/bin/sh

. "$PWD/../environment.sh"

CONTROLLER=$IP_ADDRESS
HOST=$1
SERVER=$2
GROUP=$3
OFFSET=$4
CLI=$JBOSS_HOME/bin/jboss-cli.sh

set -x

$CLI -c --controller=$CONTROLLER --command="/host=$HOST:reload()"
$CLI -c --controller=$CONTROLLER --command="/host=$HOST/server-config=$SERVER:add(auto-start=true, group=$GROUP, socket-binding-port-offset=$OFFSET)"
$CLI -c --controller=$CONTROLLER --command="/host=$HOST/server-config=$SERVER:start()"
