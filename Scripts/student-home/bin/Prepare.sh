#!/bin/bash

# TODO test STUDENT_HOME is defined or quit

SOFTWARE=$STUDENT_HOME/Software
PACKAGES=$STUDENT_HOME/Packages

if [ "$1" == "-y" ]; then
    REPLY=y
else 
    echo 'WARNING: ARE YOU REALLY GOING TO RESET THE LABORATORY? THIS WILL ERASE ALL YOUR CHANGES.'
    read -r -p "Are tou sure? Enter 'y' to proceed and press enter: " REPLY
fi


if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "Removing $SOFTWARE and $STUDENT_HOME/Lab ..."
    rm -rf $SOFTWARE $STUDENT_HOME/Lab
    mkdir -p $SOFTWARE

    echo "Installing JDK 8..."
    tar xf $PACKAGES/jdk-8u251-linux-x64.tar.gz -C $SOFTWARE
    echo "Installing JBoss EAP 7.3"
    unzip -q $PACKAGES/jboss-eap-7.3.0.zip -d $SOFTWARE
    echo "Installing Apache Maven 3.6.3 ..."
    tar xf $PACKAGES/apache-maven-3.6.3-bin.tar.gz -C $SOFTWARE
    echo "Installing RHAMT 4.3.1 ..."
    unzip -q $PACKAGES/migrationtoolkit-rhamt-cli-4.3.1-light.zip -d $SOFTWARE

    echo "Installing materials for exercises ..."
    unzip -q $PACKAGES/jboss-eap7-adv-admin-labs -d $STUDENT_HOME

    echo "Configuring JBoss EAP administrative login as admin/admin..."
    (cd $STUDENT_HOME; patch -p0 -R < $STUDENT_HOME/Packages/jboss-admin.patch)
fi

echo
echo "Folder layout for this classroom:"
echo "\$HOME/Packages   - contains binaries for all needed software pieces."
echo "\$HOME/Software   - contains installations of all needed software."
echo "\$HOME/Lab        - contains materials for exercises."
echo 
echo "Every time you want to reset to the original state, run Prepare.sh"
