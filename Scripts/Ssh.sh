#!/bin/bash

readonly VERSION=1.6
readonly VM_NAME="JBoss Advanced Administration v$VERSION"
readonly ADMIN_USER=administrator
readonly ADMIN_PASSWORD=admin
readonly GUEST_IP=`VBoxManage guestcontrol "$VM_NAME" run --username root --password $ADMIN_PASSWORD --wait-stdout -- /sbin/ip address show dev enp0s8 | grep "inet " | sed 's/^.*inet //g' | sed 's/\/.*$//g' | tr -cd '[:print:]'`
echo "Guest public IP address is $GUEST_IP"
ssh -v -q -o "StrictHostKeyChecking=no" student@$GUEST_IP "$@"
