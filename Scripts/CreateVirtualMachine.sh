#!/bin/bash -e

# Requires that root on the target VM has been given a password 'admin'

PROJECT_HOME="$HOME/Business/Tidalwave/Courseware/RedHat_JBoss_EAP_7_Administration"
APPLIANCES_PATH="$HOME/Business/VirtualBox VMs/Appliances/"
BASE_VM_NAME="Tidalwave/Linux/Ubuntu Desktop 18.04 x64 2020-07-11.ova"

readonly VERSION=1.6
readonly VM_NAME="JBoss Advanced Administration v$VERSION"
readonly CPU_COUNT=2
readonly MEMORY_SIZE=4
readonly WORKAREA_VOLUME_SIZE=30
readonly SWAP_SIZE=8
readonly HOST_NAME=Workstation
readonly ID_RSA_PUB=`cat $HOME/.ssh/id_rsa.pub`

readonly LVM_NAME=workarea
readonly WORKAREA_VOLUME_NAME="$VM_NAME-Workarea.vdi"
readonly MOUNT_POINT=/media/workarea
readonly ALT_HOME=$MOUNT_POINT/home

readonly ADMIN_USER=administrator
readonly ADMIN_PASSWORD=admin
readonly STUDENT_USER=student
readonly STUDENT_PASSWORD=student
readonly STUDENT_HOME2=$ALT_HOME/$STUDENT_USER

readonly SCRIPT="SCRIPT.sh"

case `uname` in
    "Darwin")
        # headless doesn't work on macOS
        readonly START_TYPE=gui
        ;;
    *)
        readonly START_TYPE=headless
        ;;
esac

#
# Takes a snapshot.
#
# $1: name of the snapshot
#
function takeSnapshot
  {
    local SNAPSHOT_NAME="$1"
    echo "Taking snapshot $SNAPSHOT_NAME"
    VBoxManage snapshot "$VM_NAME" take "$SNAPSHOT_NAME"
  }

#
# Imports the VM from the $BASE_VM_NAME and performs some basic configuration.
#
function importVirtualMachine
  {
    echo "Removing old instance..."
    VBoxManage unregistervm "$VM_NAME" --delete || true
    echo "Importing base VM..."
    VBoxManage import --vsys 0 --unit 9 --ignore --vmname "$VM_NAME" "$APPLIANCES_PATH/$BASE_VM_NAME"
#    VBoxManage import --vsys 0 --unit 9 --ignore  --options importtovdi --vmname "$VM_NAME" "$APPLIANCES_PATH/$BASE_VM_NAME"
    takeSnapshot "After import from base VM"

    VBoxManage modifyvm "$VM_NAME" --description "Administrative account: administrator/admin, working account: student/student"
    VBoxManage modifyvm "$VM_NAME" --cpus $CPU_COUNT
    VBoxManage modifyvm "$VM_NAME" --memory $(($MEMORY_SIZE * 1024))
    VBoxManage modifyvm "$VM_NAME" --draganddrop bidirectional
    VBoxManage sharedfolder remove "$VM_NAME" --name "VirtualBox_Shared_Folder"
    VBoxManage sharedfolder add "$VM_NAME" --name "LabSources" --automount --hostpath "$PROJECT_HOME/Sources"
    enableBridgedNetworking

    takeSnapshot "After configuration ($MEMORY_SIZE GB, $CPU_COUNT CPU)"
  }

#
# Enables the bridged networking binding all the available network cards.
#
function enableBridgedNetworking
  {
    echo "Configuring the bridged adapters..."

    case `uname` in
        "Darwin")
            VBoxManage modifyvm "$VM_NAME" --nic2 bridged
            VBoxManage modifyvm "$VM_NAME" --cableconnected2 on
            VBoxManage modifyvm "$VM_NAME" --bridgeadapter2 en0
            VBoxManage modifyvm "$VM_NAME" --nic3 bridged
            VBoxManage modifyvm "$VM_NAME" --cableconnected3 on
            VBoxManage modifyvm "$VM_NAME" --bridgeadapter3 en1
            VBoxManage modifyvm "$VM_NAME" --nic4 bridged
            VBoxManage modifyvm "$VM_NAME" --cableconnected4 on
            VBoxManage modifyvm "$VM_NAME" --bridgeadapter4 en2
            ;;
        "Linux")
            VBoxManage modifyvm "$VM_NAME" --nic2 bridged
            VBoxManage modifyvm "$VM_NAME" --cableconnected2 on
            VBoxManage modifyvm "$VM_NAME" --bridgeadapter2 enp2s0f0
            ;;
    esac
  }

#
# Disables the bridged networking binding all the available network cards.
#
function disableBridgedNetworking
  {
    echo "Removing the bridged adapters..."
    VBoxManage modifyvm "$VM_NAME" --nic2 none
    VBoxManage modifyvm "$VM_NAME" --nic3 none
    VBoxManage modifyvm "$VM_NAME" --nic4 none
  }

#
# Creates the 'workarea' medium and attaches it.
#
function addWorkareaMedium
  {
    readonly VM_FOLDER=/`VBoxManage showvminfo "$VM_NAME" | grep "SATA (0, 0)" | sed 's/SATA.*: \///' | sed 's/ (UUID.*//' | sed 's/\(.*\/\).*/\1/'`
    readonly WORKAREA_VOLUME_PATH="$VM_FOLDER$WORKAREA_VOLUME_NAME"

    set +e
    echo "Removing old workarea disk... at $WORKAREA_VOLUME_PATH"
    VBoxManage storageattach "$VM_NAME" --storagectl SATA --port 1 --type hdd --medium none
    VBoxManage closemedium --delete "$WORKAREA_VOLUME_PATH"
    set -e
    echo "Creating a new workarea disk of $WORKAREA_VOLUME_SIZE GB..."
    VBoxManage createmedium disk --size $(($WORKAREA_VOLUME_SIZE * 1024)) --filename "$WORKAREA_VOLUME_PATH" --format VDI --variant standard
    readonly WORKAREA_VOLUME_UUID=`VBoxManage showmediuminfo "$WORKAREA_VOLUME_PATH" | grep "^UUID:" | sed 's/UUID: *//'`
    echo "Attaching workarea disk..."
    VBoxManage storageattach "$VM_NAME" --storagectl SATA --port 1 --type hdd --medium $WORKAREA_VOLUME_UUID
    takeSnapshot "After workarea media attached ($MEMORY_SIZE GB, $CPU_COUNT CPU)"
  }

#
# Runs a script on the guest using the 'guestcontrol' feature of VirtualBox (doesn't use ssh). The script is first
# copied to the guest and then run as the $ADMIN_USER account. The script is removed after execution.
#
# $1: the file of the script
#
function runScriptOnGuest
  {
    local _SCRIPT=$1
    VBoxManage guestcontrol "$VM_NAME" copyto --username $ADMIN_USER --password $ADMIN_PASSWORD --target-directory /home/$ADMIN_USER "$_SCRIPT"
    VBoxManage guestcontrol "$VM_NAME" run --username root --password $ADMIN_PASSWORD --wait-stdout -- /bin/bash /home/$ADMIN_USER/SCRIPT.sh
    VBoxManage guestcontrol "$VM_NAME" run --username root --password $ADMIN_PASSWORD --wait-stdout -- /bin/rm /home/$ADMIN_USER/SCRIPT.sh
    rm $_SCRIPT
  }

#
# Starts the virtual machine and waits until it's able to execute commands.
#
function startVirtualMachine
  {
    echo "Starting the virtual machine..."
    VBoxManage startvm "$VM_NAME" --type $START_TYPE
    local STATUS=0

    set +e
    until [ $STATUS -ne 0 ]; do
        echo "Waiting for the VM to be ready..."
        2>&1 VBoxManage guestcontrol "$VM_NAME" run --username $ADMIN_USER --password $ADMIN_PASSWORD --wait-stdout -- /bin/echo | grep -q "The guest execution service is not ready"
        STATUS=$?
        sleep 1
    done
    set -e
    sleep 5
  }

#
# Shutd down the virtual machine and waits until it's powered off.
#
function stopVirtualMachine
  {
    echo "Stopping the virtual machine..."
    VBoxManage controlvm "$VM_NAME" acpipowerbutton

    until $(VBoxManage showvminfo --machinereadable "$VM_NAME" | grep -q ^VMState=.poweroff.); do
        echo "Waiting for power off..."
        sleep 1
    done

    sleep 2
  }

#
# Prepares the virtual machine by creating the workarea and swap volumes, creating the 'student' user and installing
# the OpenSSH server.
#
function prepareVirtualMachine
  {
    cat > "$SCRIPT" << EOF
    echo $HOST_NAME > /etc/hostname
    hostname $HOST_NAME
    echo "Initializing and mounting workarea disk..."
    pvcreate /dev/sdb
    vgcreate $LVM_NAME-vg /dev/sdb
    lvcreate -n swap -L ${SWAP_SIZE}G $LVM_NAME-vg
    lvcreate -n $LVM_NAME -l 100%FREE $LVM_NAME-vg
    mkfs.ext4 /dev/$LVM_NAME-vg/$LVM_NAME
    mkswap /dev/$LVM_NAME-vg/swap
    mkdir $MOUNT_POINT
    echo /dev/mapper/$LVM_NAME--vg-swap none swap sw 0 0 >> /etc/fstab
    echo /dev/mapper/$LVM_NAME--vg-$LVM_NAME $MOUNT_POINT ext4 errors=remount-ro 0 0 >> /etc/fstab
    echo "Mounting workarea disk..."
    mount -av
    echo "Activating new swap..."
    swapon -a
    swapon --show

    echo "Creating account $STUDENT_USER..."
    mkdir $ALT_HOME
    chmod a+rx $ALT_HOME
    useradd -d $STUDENT_HOME2 -m -s /bin/bash -c "Student" $STUDENT_USER
    echo "$STUDENT_USER:$STUDENT_PASSWORD" | chpasswd
    ln -s $STUDENT_HOME2 /home/$STUDENT_USER
    adduser $STUDENT_USER sudo
    usermod -G vboxsf -a $STUDENT_USER

    echo "Setting up OpenSSH server..."
    mkdir $STUDENT_HOME2/.ssh
    echo $ID_RSA_PUB > $STUDENT_HOME2/.ssh/authorized_keys
    chown $STUDENT_USER:$STUDENT_USER -R $STUDENT_HOME2/.ssh
    chmod 700 $STUDENT_HOME2/.ssh
    chmod 600 $STUDENT_HOME2/.ssh/authorized_keys
    apt update
    apt -y install openssh-server
    echo "Installing other packages..."
    apt -y install nmap git tcsh
EOF

    runScriptOnGuest $SCRIPT
    takeSnapshot "After openssh installed ($MEMORY_SIZE GB, $CPU_COUNT CPU)"
  }

#
# Performs additional setup.
#
function doAdditionalSetup
  {
    cat > "$SCRIPT" << EOF
    echo "Installing other packages..."
    apt -y install apache2 mysql-server
    systemctl stop apache2
    systemctl disable apache2
    systemctl stop mysql
    systemctl disable mysql
EOF

    runScriptOnGuest $SCRIPT
    takeSnapshot "After additional setup ($MEMORY_SIZE GB, $CPU_COUNT CPU)"
  }

#
# Copies the 'student' materials.
#
function prepareStudentStuff
  {
    readonly DEVICE_NAME=enp0s8
    readonly GUEST_IP=`VBoxManage guestcontrol "$VM_NAME" run --username root --password $ADMIN_PASSWORD --wait-stdout -- /sbin/ip address show dev $DEVICE_NAME | grep "inet " | sed 's/^.*inet //g' | sed 's/\/.*$//g' | tr -cd '[:print:]'`
    echo "Guest public IP address is $GUEST_IP"

    readonly SSH_OPTIONS="-q -o StrictHostKeyChecking=no"

    rsync -av --progress -e "ssh $SSH_OPTIONS" --exclude .DS_Store student-home/ $STUDENT_USER@$GUEST_IP:
    ssh $SSH_OPTIONS $STUDENT_USER@$GUEST_IP echo '. $HOME/.bashrc_extra >> .bashrc'
    ssh $SSH_OPTIONS $STUDENT_USER@$GUEST_IP rm -rf "Documents Music Pictures Public Templares Videos examples.desktop"

    takeSnapshot "After student preparation ($MEMORY_SIZE GB, $CPU_COUNT CPU)"
  }

#
# Clears the virtual machine by removing useless stuff and zeroing volumes.
#
function cleanVirtualMachine
  {
    cat > "$SCRIPT" << EOF
    echo "Removing stuff in $STUDENT_HOME2 ..."
    cd $STUDENT_HOME2
    rm -rf Software Labs .java .mozilla .rhamt .bash_history .jboss-cli-history .mysql_history .visualvm

    echo "Removing stuff in /home/$ADMIN_USER ..."
    cd /home/$ADMIN_USER
    rm -rf .java .mozilla .rhamt .bash_history .jboss-cli-history .mysql_history .visualvm

    echo "Cleaning APT cache ..."
    apt clean

    echo "Zeroing primary disk ..."
    # Running as root, we should avoid a disk full on this partition...
    dd if=/dev/zero of=/tmp/zero bs=1024 count=125829120
    rm /tmp/zero

    echo "Zeroing workarea disk ..."
    dd if=/dev/zero of=$MOUNT_POINT/zero bs=1024 count=125829120
    rm $MOUNT_POINT/zero
EOF

    runScriptOnGuest $SCRIPT
  }

#
# Exports the virtual machine.
#
function exportVirtualMachine
  {
    takeSnapshot "Before clean up for export ($MEMORY_SIZE GB, $CPU_COUNT CPU)"
    sleep 1
    disableBridgedNetworking

    # TODO set display scaling to 100%

    echo "Removing the shared folder..."
    set +e
    VBoxManage sharedfolder remove "$VM_NAME" --name "LabSources"
    set -e

    local OVA_PATH="$PROJECT_HOME/Appliances/$VM_NAME.ova"
    echo "Exporting to $OVA_PATH ..."
    rm -f "$OVA_PATH"
    VBoxManage export "$VM_NAME" -o "$OVA_PATH" --ovf20 # --vsys --vmname "$VM_NAME"
    du -h "$OVA_PATH"
  }

#
# Posts a notification to the user.
#
function notification()
  {
    case `uname` in
        "Darwin")
            osascript -e "display notification \"$1\" with title \"CreateVirtualMachine.sh\""
            ;;
        "Linux")
            ;;
    esac
  }

#
# Run option
#
function runOption()
  {
    local OPTION=$1

    case $OPTION in
        "Create the appliance")
            importVirtualMachine
            addWorkareaMedium
            startVirtualMachine
            prepareVirtualMachine
            doAdditionalSetup
            prepareStudentStuff
            cleanVirtualMachine
            stopVirtualMachine
            exportVirtualMachine
            ;;

        "Create the virtual machine")
            importVirtualMachine
            addWorkareaMedium
            startVirtualMachine
            prepareVirtualMachine
            doAdditionalSetup
            prepareStudentStuff
            ;;

        "Export the appliance")
            startVirtualMachine
            cleanVirtualMachine
            stopVirtualMachine
            exportVirtualMachine
            ;;

        "Prepare student stuff")
            prepareStudentStuff
            ;;

        "Enable bridged networking")
            enableBridgedNetworking
            ;;

        "Disable bridged networking")
            disableBridgedNetworking
            ;;

        *) echo "Invalid option: $OPTION"
            ;;
    esac
  }

#
# main
#
for i in "$@"; do
    case $i in
        --batch=*)
            OPTION="${i#*=}"
            shift
            ;;
        --projectHome=*)
            PROJECT_HOME="${i#*=}"
            echo "PROJECT_HOME: $PROJECT_HOME"
            shift
            ;;
        --appliancePath=*)
            APPLIANCES_PATH="${i#*=}"
            echo "APPLIANCES_PATH: $APPLIANCES_PATH"
            shift
            ;;
        --baseVM=*)
            BASE_VM_NAME="${i#*=}"
            echo "BASE_VM_NAME: $BASE_VM_NAME"
            shift
            ;;
    esac
done

if [[ -z "$OPTION" ]]; then
    PS3='Please enter your choice: '
    OPTIONS=("Create the appliance" \
             "Create the virtual machine" \
             "Export the appliance" \
             "Prepare student stuff" \
             "Enable bridged networking" \
             "Disable bridged networking" \
             "Quit")

    select OPTION in "${OPTIONS[@]}"; do
        if [[ "$OPTION" == "Quit" ]]; then
            break
        fi

        notification "Starting $OPTION ..."
        runOption "$OPTION"
        notification "Done $OPTION"
    done
else
    runOption "$OPTION"
fi

