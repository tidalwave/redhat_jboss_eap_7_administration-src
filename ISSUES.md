PENDING
=======

EAP7AA-2:   Locale is italian.
EAP7AA-3:   The workarea volume is labeled "75 GB"
EAP7AA-7:   load-balancing exercise: deploy also square.war
EAP7AA-8:   load-balancing exercise: at the end disable HTTP ports in slaves

********
EAP7AA-9:   Move rolling-update in a separate folder; make a script to scratch main-group and other group and
            create two groups from full; also enable AJP connectors.
********

EAP7AA-10:  Also distribute sources of apps; and fix the apps pom so it depends on the tidalwave SuperPOM.

EAP7AA-12:  Rewrite the slide about mod_jk and mod_cluster deprecations:
            https://access.redhat.com/documentation/en-us/red_hat_jboss_enterprise_application_platform/7.0/html/7.0.0_release_notes/release_notes_new_features#changes_to_delivery_of_eap_natives_and_apache_http_server
EAP7AA-13:  Add to slide "Funzionalità addizionali":
            https://access.redhat.com/documentation/en-us/red_hat_jboss_core_services/2.4.23/html-single/apache_http_server_installation_guide/index#about_red_hat_jboss_core_services
EAP7AA-14:  Install telnet.
EAP7AA-15:  Slide Wildfly vs JBoss.
EAP7AA-16:  Add slide to explain the four profiles (full, ha, etc...)
EAP7AA-17:  Say that the two predefined server groups have different profiles.

EAP7AA-18:  Add exercise on how to run EAP 7 as a Linux service.

EAP7AA-19:  CLI additional exercise: show how to add and edit system properties from CLI and GUI.

EAP7AA-20:  In the exercise when logging is changed, say that on the console DEBUG isn't seen because of an override.

********
EAP7AA-21:  Add exercise for using a database as an application realm.
********

********
EAP7AA-22:  Fix Artemis exercise.
********

********
EAP7AA-23:  Add SSO exercise.
********

EAP7AA-24:  Improve initial desktop configuration by dumping/importing the configuration database. See 
            https://askubuntu.com/questions/805144/how-do-i-restore-mate-panel-settings-from-old-backup


FIXED
=====

EAP7AA-1:   Should configure MATE as default login configuration.
EAP7AA-4:   Should set dark background.
EAP7AA-5:   Should set dark style.
EAP7AA-6:   Missing terminal icon on the topbar.
EAP7AA-11:  Disable install in the maven project.
