package eap7.square;

import java.io.PrintWriter;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.stats.CacheContainerStats;

public class ResourceSupport {
    
    @Resource(lookup = "java:jboss/infinispan/container/calccontainer")
    protected EmbeddedCacheManager cacheManager;

    protected Cache<Integer, Integer> cache;

    @PostConstruct
    private void initialize() {
        try {
            System.out.println("SQUARE: CACHES " + cacheManager.getCacheNames());
            System.out.println("SQUARE: STATUS " + cacheManager.getStatus());
            System.out.println("SQUARE: CONF   " + cacheManager.getCacheConfigurationNames());
            cache = cacheManager.getCache();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    protected void printStats(final PrintWriter out, final EmbeddedCacheManager cc) {
        final CacheContainerStats stats = cc.getStats();
        out.println("SQUARE: STATS HITS:          " + stats.getHits());
        out.println("SQUARE: STATS MISSES:        " + stats.getMisses());
        out.println("SQUARE: STATS ENTRIES:       " + stats.getCurrentNumberOfEntries());
        out.println("SQUARE: STATS TOTAL ENTRIES: " + stats.getTotalNumberOfEntries());
        out.println("SQUARE: STATS MEMORY USED:   " + stats.getDataMemoryUsed());
        out.println("SQUARE: STATS HIT RATIO:     " + stats.getHitRatio());
        out.println("SQUARE: STATS EVICTIONS:     " + stats.getEvictions());
        out.println("SQUARE: STATS R/W RATIO:     " + stats.getReadWriteRatio());
        out.flush();
    }
}
