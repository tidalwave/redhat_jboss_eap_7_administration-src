package eap7.square;

import java.io.PrintWriter;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

@Path("calc")
@ApplicationScoped
public class SquareResource extends ResourceSupport {

    @Path("{n}")
    @GET
    public String get(final @PathParam("n") int n, final @QueryParam("fast") @DefaultValue("false") boolean fast) {
        System.out.println("SQUARE: GET" + n);
        String r = "";

        if (cache.containsKey(n)) {
            System.out.printf("%d is in cache\n", n);
            r = "cached " + cache.get(n);
        } else {
            System.out.printf("%d is NOT in cache\n", n);

            if (!fast) {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException ex) {
                }
            }

            int v = n * n;
            cache.put(n, v);
            r = "computed " + v;
        }

        printStats(new PrintWriter(System.out), cacheManager);

        return r;
    }

}
