package eap7.square;

import java.io.PrintWriter;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("clear")
@ApplicationScoped
public class ClearResource extends ResourceSupport {

    @GET
    public String get() {
        System.out.println("SQUARE: CLEAR");
        cache.clear();
        printStats(new PrintWriter(System.out), cacheManager);
        
        return "Cache cleared";
    }
}
