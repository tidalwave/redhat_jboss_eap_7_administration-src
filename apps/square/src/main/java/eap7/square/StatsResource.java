package eap7.square;

import java.io.PrintWriter;
import java.io.StringWriter;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("stats")
@ApplicationScoped
public class StatsResource extends ResourceSupport {

    @GET
    public String get() {
        System.out.println("SQUARE: STATS");
        final StringWriter sw = new StringWriter();
        printStats(new PrintWriter(sw), cacheManager);
        return sw.toString();
    }
}
