package eap7.jms.client;

import java.util.Properties;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Main {

    private static final String DEFAULT_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";

    private static final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
//    private static final String INITIAL_CONTEXT_FACTORY = "org.wildfly.naming.client.WildFlyInitialContextFactory";

    public static void main(final String[] args) throws JMSException, NamingException {
        final String url = System.getProperty("jmsclient.url");
        final String userName = System.getProperty("jmsclient.userName");
        final String password = System.getProperty("jmsclient.password");
        final String queueName = System.getProperty("jmsclient.queueName");
        final String command = System.getProperty("jmsclient.command");

        final Properties env = new Properties();
        env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_PRINCIPAL, userName);
        env.put(Context.SECURITY_CREDENTIALS, password);
        final Context namingContext = new InitialContext(env);
        final ConnectionFactory factory = (ConnectionFactory) namingContext.lookup(DEFAULT_CONNECTION_FACTORY);

        try (final Connection conn = factory.createConnection(userName, password);
             final Session session = conn.createSession(false, Session.CLIENT_ACKNOWLEDGE)) {
            final Queue queue = (Queue) namingContext.lookup(queueName);
            
            if ("send".equals(command)) {
                final MessageProducer producer = session.createProducer(queue);
                final Message message = session.createTextMessage(System.getProperty("jmsclient.message"));
                System.out.println("Sending " + message);
                producer.send(message);
            } else if ("send-expiring".equals(command)) {
                final MessageProducer producer = session.createProducer(queue);
                producer.setTimeToLive(10000);
                final Message message = session.createTextMessage(System.getProperty("jmsclient.message"));
                System.out.println("Sending " + message + " with expiration");
                producer.send(message);
            } else {
                conn.start();

                while (true) {
                    final MessageConsumer consumer = session.createConsumer(queue);
                    final Message message = consumer.receiveNoWait();

                    if (message == null) {
                        break;
                    }

                    if (!(message instanceof TextMessage)) {
                        throw new RuntimeException("Expected a TextMessage");
                    }

                    TextMessage textMessage = (TextMessage) message;
                    final String text = textMessage.getText();
                    System.out.println(text);

                    if (!text.startsWith("poison") || queueName.endsWith("DLQ")) {
                        message.acknowledge();
                    } else {
                        System.out.println("Simulating error in reception.");
                    }
                }
            }
        }
    }
}
